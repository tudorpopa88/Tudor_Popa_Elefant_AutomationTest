package com.elefant.automation.pages;

import com.elefant.automation.utils.ScreenshotGenerator;
import com.elefant.automation.utils.ThreadSleeper;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;

// This class contains the Search test procedure
public class SearchPage {

    // The search box (Main Page)
    @FindBy (how = How.ID, using = "query")
    private WebElement searchBox;

    // The search error message (Search results Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='error-template']/h2")
    private List<WebElement> errorMsg;


    /**This method contains the Search test procedure with valid and invalid inputs and then asserts the error message
     *
     * @param driver is the WebDriver
     * @param name is the search query
     * @param searchError is the search error message
     * @param screenshotNumber is an index variable used to differentiate the screenshot files when receiving data from the Data Provider
     * @throws IOException exception
     */
    public void search (WebDriver driver, String name, String searchError, String screenshotNumber) throws IOException {
        searchBox.click();
        searchBox.sendKeys(name);
        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_searchTest_" + screenshotNumber + ".png");
        searchBox.sendKeys(Keys.ENTER);
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_searchTest_" + (Integer.parseInt(screenshotNumber)+10) + ".png");

        ThreadSleeper.mySleeper(1000L);
        if (errorMsg.size() > 0) {
            Assert.assertEquals(errorMsg.get(0).getText(), searchError);
        }
        driver.navigate().back();
    }
}
