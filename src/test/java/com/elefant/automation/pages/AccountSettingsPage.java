package com.elefant.automation.pages;

import com.elefant.automation.utils.ScreenshotGenerator;
import com.elefant.automation.utils.ThreadSleeper;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;
import java.util.Random;

// This class contains the Account settings test procedure
public class AccountSettingsPage {

    // The female gender button (Account settings Page)
    @FindBy (how = How.XPATH, using = ".//*[@id='sex_1']")
    private WebElement genderFemale;

    // The male gender button (Account settings Page)
    @FindBy (how = How.XPATH, using = ".//*[@id='sex_2']")
    private WebElement genderMale;

    // The first name field (Account settings Page)
    @FindBy (how = How.ID, using = "firstname")
    private WebElement firstName;

    // The last name field (Account settings Page)
    @FindBy (how = How.ID, using = "lastname")
    private WebElement lastName;

    // The email field (Account settings Page)
    @FindBy (how = How.ID, using = "email")
    private WebElement email;

    // The day of birth selector (Account settings Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='form-group form-inline']/div[1]/select")
    private WebElement day;

    // The month of birth selector (Account settings Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='form-group form-inline']/div[2]/select")
    private WebElement month;

    // The year of birth selector (Account settings Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='form-group form-inline']/div[3]/select")
    private WebElement year;

    // The day of birth options from selector (Account settings Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='form-group form-inline']/div[1]/select/option")
    private List<WebElement> dayOptions;

    // The month of birth options from selector (Account settings Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='form-group form-inline']/div[2]/select/option")
    private List<WebElement> monthOptions;

    // The year of birth options from selector (Account settings Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='form-group form-inline']/div[3]/select/option")
    private List<WebElement> yearOptions;

    // The error message for empty first name (Account settings Page)
    @FindBy (how = How.ID, using = "firstname-error")
    private List<WebElement> errorFirstName;

    // The error message for empty last name (Account settings Page)
    @FindBy (how = How.ID, using = "lastname-error")
    private List<WebElement> errorLastName;

    // The error message for empty email (Account settings Page)
    @FindBy (how = How.ID, using = "email-error")
    private List<WebElement> errorEmail;

    // The information message for email changing confirmation email (Account settings Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='user-container']/div[1]")
    private List<WebElement> emailChangeValid;

    // The error message for no day of birth option selected (Account settings Page)
    @FindBy (how = How.ID, using = "zi_nastere-error")
    private WebElement errorDay;

    // The error message for no month of birth option selected (Account settings Page)
    @FindBy (how = How.ID, using = "luna_nastere-error")
    private WebElement errorMonth;

    // The error message for no year of birth option selected (Account settings Page)
    @FindBy (how = How.ID, using = "an_nastere-error")
    private WebElement errorYear;

    // The save changes button (Account settings Page)
    @FindBy (how = How.XPATH, using = ".//*[@id='detalii-cont']/input")
    private WebElement saveButton;


    /**This method contains the Account settings test procedure with valid and invalid inputs and then asserts the error messages
     *
     * @param driver is the WebDriver
     * @param firstName is the user first name
     * @param lastName is the user last name
     * @param email is the user email
     * @param errorFirstName is the error message for empty first name
     * @param errorLastName is the error message for empty last name
     * @param errorEmail is the error message for empty email
     * @param errorValidEmail is the error message for incorrect email
     * @param emailChangeValid is the information message for email changing confimration email
     * @param screenshotNumber is an index variable used to differentiate the screenshot files when receiving data from the Data Provider
     * @throws IOException exception
     */
    public void accountSettings(WebDriver driver, String firstName, String lastName, String email, String errorFirstName, String errorLastName, String errorEmail, String errorValidEmail, String emailChangeValid, String screenshotNumber) throws IOException {
        Random rand = new Random();
        int randGenderNo = rand.nextInt(2) + 1;
        if (randGenderNo % 2 == 0) {
            genderFemale.click();
        } else {
            genderMale.click();
        }
        this.firstName.clear();
        this.firstName.sendKeys(firstName);
        this.lastName.clear();
        this.lastName.sendKeys(lastName);
        this.email.clear();
        this.email.sendKeys(email);

        Select daySelector = new Select(day);
        int dayIndex = dayOptions.size();
        int randDay = rand.nextInt(dayIndex) + 1;
        String selectedDay = dayOptions.get(randDay - 1).getAttribute("value");
        daySelector.selectByValue(selectedDay);

        Select monthSelector = new Select(month);
        int monthIndex = monthOptions.size();
        int randMonth = rand.nextInt(monthIndex) + 1;
        String selectedMonth = monthOptions.get(randMonth - 1).getAttribute("value");
        monthSelector.selectByValue(selectedMonth);

        Select yearSelector = new Select(year);
        int yearIndex = yearOptions.size();
        int randYear = rand.nextInt(yearIndex) + 1;
        String selectedYear = yearOptions.get(randYear - 1).getAttribute("value");
        yearSelector.selectByValue(selectedYear);

        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_accountUpdateTest_" + (Integer.parseInt(screenshotNumber)+2) + ".png");
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0, 200)");

        ThreadSleeper.mySleeper(1000L);
        saveButton.submit();
        ThreadSleeper.mySleeper(1000L);

        if (this.errorFirstName.size() > 0) {
            if (this.errorFirstName.get(0).getText().equals("Completeaza campul de prenume!")) {
                Assert.assertEquals(this.errorFirstName.get(0).getText(), errorFirstName);
            }
        }
        if (this.errorLastName.size() > 0) {
            if (this.errorLastName.get(0).getText().equals("Completeaza campul de nume!")) {
                Assert.assertEquals(this.errorLastName.get(0).getText(), errorLastName);
            }
        }
        if (this.errorEmail.size() > 0) {
            if (this.errorEmail.get(0).getText().equals("Introdu o adresa de email!")) {
                Assert.assertEquals(this.errorEmail.get(0).getText(), errorEmail);
            }
        }
        if (this.errorEmail.size() > 0) {
            if (this.errorEmail.get(0).getText().equals("Introdu o adresa de email valida!")) {
                Assert.assertEquals(this.errorEmail.get(0).getText(), errorValidEmail);
            }
        }
        if (this.emailChangeValid.size() > 0) {
            if (this.emailChangeValid.get(0).getText().equals("Ai primit un mesaj pe adresa ta noua, te rugam sa validezi schimbarea adresei de email pentru contul elefant.ro.")) {
                Assert.assertEquals(this.emailChangeValid.get(0).getText(), emailChangeValid);
            }
        }
    }
}
