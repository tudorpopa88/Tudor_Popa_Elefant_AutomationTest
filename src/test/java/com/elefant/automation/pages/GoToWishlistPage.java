package com.elefant.automation.pages;

import com.elefant.automation.utils.ScreenshotGenerator;
import com.elefant.automation.utils.ThreadSleeper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.io.IOException;

// This class contains the navigation to Wishlist Page
public class GoToWishlistPage {

    // The enter wishlist button (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='header-wishlist-icon']")
    private WebElement enterWishlist;

    // The wishlist title button (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='title']/a")
    private WebElement wishlistTitle;


    /**This method contains the navigation to wishlist page procedure and asserts that the page title is changed accordingly
     *
     * @param driver is the WebDriver
     * @throws IOException exception
     */
    public void goToWishlist(WebDriver driver) throws IOException {
        enterWishlist.click();
        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_wishlistTest_1.png");
        ThreadSleeper.mySleeper(2000L);
        wishlistTitle.click();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_wishlistTest_2.png");
        String pageTitle = driver.getTitle();
        Assert.assertEquals(pageTitle, "Wishlist Wishlist | elefant.ro");
    }
}
