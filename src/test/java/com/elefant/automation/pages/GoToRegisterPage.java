package com.elefant.automation.pages;

import com.elefant.automation.utils.ScreenshotGenerator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.io.IOException;

// This class contains the navigation to Register Page
public class GoToRegisterPage {

    // The account menu selector (Main Page)
    @FindBy(how = How.XPATH, using = ".//*[@class='header-account-holder']/div[2]/span")
    private WebElement headerCat;

    // The Create Account option from the account menu (Main Page)
    @FindBy(how = How.XPATH, using = ".//*[@class='dropdown-menu header-account-menu']/li[3]/a")
    private WebElement newAcc;

    // The Google Plus page opener (Register Page)
    @FindBy (how = How.CLASS_NAME, using = "login-google-button")
    private WebElement enterGoogle;

    // The new account register title (Register Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='form-box ']/div[1]/div/h3")
    private WebElement registerTitle;


    /**This method navigates to Register Page and then asserts that the Register title is displayed on the page
     *
     * @param driver is the WebDriver
     * @throws IOException exception
     */
    public void goToRegister(WebDriver driver) throws IOException {
        headerCat.click();
        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_registerTest_1.png");
        newAcc.click();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_registerTest_2.png");
        enterGoogle.click();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_registerTest_3.png");
        driver.navigate().back();
        Assert.assertEquals(registerTitle.getText(), "Noul tau cont");
    }
}
