package com.elefant.automation.pages;

import com.elefant.automation.utils.ScreenshotGenerator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.io.IOException;

// This class contains the navigation to Shopping basket
public class GoToBasketPage {

    // The cart opener button (Main Page)
    @FindBy(how = How.XPATH, using = ".//*[@id='cart_count']/parent::div")
    private WebElement enterCart;

    // The cart counter number from the cart icon (Main Page)
    @FindBy(how = How.ID, using = "cart_count")
    private WebElement cartCount;

    // The cart title (Shopping basket Page)
    @FindBy(how = How.XPATH, using = ".//*[@id='main_comanda']/div[1]/div/h3")
    private WebElement cartTitle;


    /**This method navigates to Shopping Basket and then asserts that the cart title is correct
     *
     * @param driver is the WebDriver
     * @return Number of Products which is used inside the Basket Page for assertion
     * @throws IOException exception
     */
    public int goToBasket(WebDriver driver) throws IOException {
        int noOfProducts = Integer.parseInt(cartCount.getText());
        enterCart.click();
        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_shoppingBasketTest_1.png");
        Assert.assertEquals(cartTitle.getText(), "Sumar comanda");
        return noOfProducts;
    }
}
