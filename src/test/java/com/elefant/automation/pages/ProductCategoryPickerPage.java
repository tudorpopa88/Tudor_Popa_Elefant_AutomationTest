package com.elefant.automation.pages;

import com.elefant.automation.utils.ScreenshotGenerator;
import com.elefant.automation.utils.ThreadSleeper;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;

// This class contains the Product category picker, Product picker and Add to wishlist procedures
public class ProductCategoryPickerPage {

    // The main category list (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@id='navbar-collapse-grid']/ul/li")
    private List<WebElement> primaryCat;

    // The main category list titles (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@id='navbar-collapse-grid']/ul/li[*]/a")
    private List<WebElement> primaryCatTitle;

    // The subcategory list (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='dropdown-menu']/div/div[*]/a[1]")
    private List<WebElement> secondaryCat;

    // The products titles (Page of products)
    @FindBy (how = How.XPATH, using = ".//*[@class='item col-xs-6 col-sm-4 col-md-4 elf-product']/div/div[2]/div/div[2]/a")
    private List<WebElement> products;

    // The add to cart button (Selected product page)
    @FindBy (how = How.XPATH, using = ".//*[@id='add-to-cart']/span[1]")
    private WebElement addToCart;

    // The size selector (Selected product page)
    @FindBy (how = How.XPATH, using = ".//*[@class='product-sizes']/select")
    private WebElement sizeSelect;

    // The size options from selector (Selected product page)
    @FindBy (how = How.XPATH, using = ".//*[@class='product-change-size']")
    private List<WebElement> sizeOptions;

    // The add to wishlist button (Selected product page)
    @FindBy (how = How.XPATH, using = ".//*[@id='add-to-wishlist']/span[1]")
    private WebElement addToWishlist;

    // The add to specific wishlist button (Selected product page)
    @FindBy (how = How.XPATH, using = ".//*[@id='add-to-wishlist']/following-sibling::ul/li[3]")
    private WebElement addToSpecificWishlist;

    // The product title (Selected product page)
    @FindBy (how = How.CLASS_NAME, using = "title")
    private WebElement productTitle;

    // The close button from cookie consent banner (Any page)
    @FindBy (how = How.XPATH, using = ".//*[@class=\"cc_banner-wrapper \"]/div/a[1]")
    private List<WebElement> cookieButton;

    // The close button from Advisor Message (Any page)
    @FindBy (how = How.XPATH, using = ".//*[@class='intercom-note-close exponea-close']")
    private List<WebElement> closeAdvisorMessage;

    /**This method contains the product category picker procedure
     * It selects a random main category and then a random subcategory
     * It avoids selecting no link subcategories (which have links only for their sub-subcategories) and empty subcategories (which have products only for their sub-subcategories)
     *
     * @param actions is the Actions driver
     * @param driver is the WebDriver
     * @param screenshotNumber is an index variable used to differentiate the screenshot files when receiving data from the Data Provider
     * @throws IOException
     */
    public void productCatPicker(Actions actions, WebDriver driver, int screenshotNumber) throws IOException {
        ThreadSleeper.mySleeper(2000L);
        Random rand = new Random();
        int primaryCatNo = primaryCatTitle.size();
        int selectedPrimaryNo = rand.nextInt(primaryCatNo) + 1;
        while (primaryCatTitle.get(selectedPrimaryNo - 1).getText().equals("Elefant Premium") ||
                primaryCatTitle.get(selectedPrimaryNo - 1).getText().equals("Carti") ||
                primaryCatTitle.get(selectedPrimaryNo - 1).getText().equals("Casa&Petshop")) {
            primaryCatNo = primaryCatTitle.size();
            selectedPrimaryNo = rand.nextInt(primaryCatNo) + 1;
        }
        System.out.println("The selected primary category is " + primaryCatTitle.get(selectedPrimaryNo - 1).getText());
        actions.moveToElement(primaryCatTitle.get(selectedPrimaryNo - 1)).build().perform();
        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_prodCatPicker_" + screenshotNumber + ".png");
        ThreadSleeper.mySleeper(2000L);
        int secondaryCatNo = primaryCat.get(selectedPrimaryNo - 1).findElements(By.xpath(".//*[@class='dropdown-menu']/div/div[*]/a[1]")).size();
        int selectedSecondaryNo = rand.nextInt(secondaryCatNo) + 1;
        while (primaryCat.get(selectedPrimaryNo - 1).findElements(By.xpath(".//*[@class='dropdown-menu']/div/div[*]/a[1]")).get(selectedSecondaryNo - 1).getAttribute("href") == null ||
                primaryCat.get(selectedPrimaryNo - 1).findElements(By.xpath(".//*[@class='dropdown-menu']/div/div[*]/a[1]")).get(selectedSecondaryNo - 1).getText().equals("Top branduri") ||
                primaryCat.get(selectedPrimaryNo - 1).findElements(By.xpath(".//*[@class='dropdown-menu']/div/div[*]/a[1]")).get(selectedSecondaryNo - 1).getText().equals("LEGO") ||
                primaryCat.get(selectedPrimaryNo - 1).findElements(By.xpath(".//*[@class='dropdown-menu']/div/div[*]/a[1]")).get(selectedSecondaryNo - 1).getText().equals("Ochelari") ||
                primaryCat.get(selectedPrimaryNo - 1).findElements(By.xpath(".//*[@class='dropdown-menu']/div/div[*]/a[1]")).get(selectedSecondaryNo - 1).getText().equals("Branduri") ||
                primaryCat.get(selectedPrimaryNo - 1).findElements(By.xpath(".//*[@class='dropdown-menu']/div/div[*]/a[1]")).get(selectedSecondaryNo - 1).getText().equals("Alte sporturi")) {
            secondaryCatNo = primaryCat.get(selectedPrimaryNo - 1).findElements(By.xpath(".//*[@class='dropdown-menu']/div/div[*]/a[1]")).size();
            selectedSecondaryNo = rand.nextInt(secondaryCatNo) + 1;
        }
        System.out.println("The selected secondary category is " + primaryCat.get(selectedPrimaryNo - 1).findElement(By.xpath(".//*[@class='dropdown-menu']/div/div[" + selectedSecondaryNo + "]/a[1]")).getText());
        primaryCat.get(selectedPrimaryNo - 1).findElement(By.xpath(".//*[@class='dropdown-menu']/div/div[" + selectedSecondaryNo + "]/a[1]")).click();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_prodCatPicker_" + (screenshotNumber+3) + ".png");
    }

    /**This method contains the product page selector procedure and then asserts that the selected product name is correct
     * It selects a random product and then do the assert
     *
     * @param driver is the WebDriver
     * @param screenshotNumber is an index variable used to differentiate the screenshot files when receiving data from the Data Provider
     * @throws IOException
     */
    public void productPagePicker(WebDriver driver, int screenshotNumber) throws IOException {
        ThreadSleeper.mySleeper(2000L);
        if (cookieButton.size() > 0) {
            cookieButton.get(0).click();
            ThreadSleeper.mySleeper(500L);
        }
        Random rand = new Random();
        int productsNo = products.size();
        int selectedProdNo = rand.nextInt(productsNo) + 1;
        String pageProductTitle = products.get(selectedProdNo - 1).getText();
        System.out.println("The selected product is " + pageProductTitle);
        products.get(selectedProdNo - 1).click();
        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_prodCatPicker_" + (screenshotNumber+6) + ".png");
        Assert.assertEquals(pageProductTitle, productTitle.getText());
    }

    /**This method contains the add to cart procedure
     * If the product is unavailable it returns to the previous page
     *
     * @param driver is the WebDriver
     */
    public void addToCart(WebDriver driver) {
        Random rand = new Random();
        ThreadSleeper.mySleeper(4000L);
        if (addToCart.getText().equals("Produs indisponibil")) {
            driver.navigate().back();
        }
        if (closeAdvisorMessage.size() > 0) {
            closeAdvisorMessage.get(0).click();
        }
        if (addToCart.getText().equals("ALEGE O MARIME")) {
            Select sizeSelector = new Select(sizeSelect);
            int sizeIndex = sizeOptions.size();
            int randSize = rand.nextInt(sizeIndex) + 1;
            String selectedSize = sizeOptions.get(randSize - 1).getText();
            sizeSelector.selectByVisibleText(selectedSize);
        }
        ThreadSleeper.mySleeper(3000L);
        addToCart.click();
        ThreadSleeper.mySleeper(1000L);
    }


    /**This method contains the add to wishlist procedure
     *
     * @param driver
     * @param actions
     * @return Product title which is used in the Wishlist Page for assertion
     * @throws IOException
     */
    public String addToWishlist(WebDriver driver, Actions actions) throws IOException {
        Random rand = new Random();
        String productTitle = this.productTitle.getText();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0, 200)");
        ThreadSleeper.mySleeper(4000L);
        if (addToCart.getText().equals("ALEGE O MARIME")) {
            Select sizeSelector = new Select(sizeSelect);
            int sizeIndex = sizeOptions.size();
            int randSize = rand.nextInt(sizeIndex) + 1;
            String selectedSize = sizeOptions.get(randSize - 1).getText();
            sizeSelector.selectByVisibleText(selectedSize);
        }
        addToWishlist.click();
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        File finalFile = new File("D:\\screenshot_addToWishlist_1.png");
        FileUtils.copyFile(screenshotFile, finalFile);
        ThreadSleeper.mySleeper(2000L);
        addToSpecificWishlist.click();
        ThreadSleeper.mySleeper(1000L);
        screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        finalFile = new File("D:\\screenshot_addToWishlist_2.png");
        FileUtils.copyFile(screenshotFile, finalFile);
        return productTitle;
    }
}
