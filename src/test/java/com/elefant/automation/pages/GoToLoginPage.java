package com.elefant.automation.pages;

import com.elefant.automation.utils.ScreenshotGenerator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.io.IOException;

// This class contains the navigation to Login Page
public class GoToLoginPage {

    // The account menu selector (Main Page)
    @FindBy(how = How.XPATH, using = ".//*[@class='header-account-holder']/div[2]/span")
    private WebElement headerCat;

    // The Enter Account option from the account menu (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='dropdown-menu header-account-menu']/li[2]/a")
    private WebElement enterAcc;

    // The Facebook page opener (Login Page)
    @FindBy (how = How.CLASS_NAME, using = "login-facebook-button")
    private WebElement enterFb;


    /**This method navigates to Login Page and then asserts that the page URL is from the Login Page
     *
     * @param driver is the WebDriver
     * @throws IOException exception
     */
    public void goToLogin(WebDriver driver) throws IOException {
        headerCat.click();
        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_loginTest_1.png");
        enterAcc.click();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_loginTest_2.png");
        enterFb.click();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_loginTest_3.png");
        driver.navigate().back();
        String pageURL = driver.getCurrentUrl();
        Assert.assertEquals(pageURL, "http://www.elefant.ro/autentificare");
    }
}
