package com.elefant.automation.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

// This class is used optionally in case of a Sale Campaign which prevents the proper loading of the Main Page
public class AvoidCampaign {

    // The back to site button (Campaign Page)
    @FindBy (how = How.XPATH, using = ".//*[@id='up']/div/div[1]/a")
    private WebElement backToSite;

    /**This method navigates to Main Page
     *
     */
    public void avoidCampaign() {
        backToSite.click();
    }
}
