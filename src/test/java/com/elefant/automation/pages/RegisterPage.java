package com.elefant.automation.pages;

import com.elefant.automation.utils.ScreenshotGenerator;
import com.elefant.automation.utils.ThreadSleeper;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;
import java.util.Random;

// This class contains the Register test procedure
public class RegisterPage {

    // The first name field (Register Page)
    @FindBy (how = How.ID, using = "r_first_name")
    private WebElement firstName;

    // The last name field (Register Page)
    @FindBy (how = How.ID, using = "r_last_name")
    private WebElement lastName;

    // The email field (Register Page)
    @FindBy (how = How.ID, using = "r_email")
    private WebElement email;

    // The password field (Register Page)
    @FindBy (how = How.ID, using = "r_password")
    private WebElement password;

    // The confirmation password field (Register Page)
    @FindBy (how = How.ID, using = "r_c_password")
    private WebElement confPass;

    // The female gender button (Register Page)
    @FindBy (how = How.ID, using = "sex_dna")
    private WebElement genderFemale;

    // The male gender button (Register Page)
    @FindBy (how = How.ID, using = "sex_dl")
    private WebElement genderMale;

    // The newsletter subscribe checkbox (Register Page)
    @FindBy (how = How.ID, using = "newsletter_subscribe")
    private WebElement newsletter;

    // The Terms and Conditions checkbox (Register Page)
    @FindBy (how = How.ID, using = "terms_ok")
    private WebElement terms;

    // The error message for incorrect gender selection (Register Page)
    @FindBy (how = How.ID, using = "r_sex-error")
    private List<WebElement> errorGender;

    // The error message for empty first name (Register Page)
    @FindBy (how = How.ID, using = "r_first_name")
    private List<WebElement> errorFirstName;

    // The error message for empty last name (Register Page)
    @FindBy (how = How.ID, using = "r_last_name")
    private List<WebElement> errorLastName;

    // The error message for empty email (Register Page)
    @FindBy (how = How.ID, using = "r_email-error")
    private List<WebElement> errorEmail;

    // The error message for empty password (Register Page)
    @FindBy (how = How.ID, using = "r_password-error")
    private List<WebElement> errorPassword;

    // The error message for empty confirmation password (Register Page)
    @FindBy (how = How.ID, using = "r_c_password-error")
    private List<WebElement> errorConfirmPass;

    // The error message when terms and conditions are not checked (Register Page)
    @FindBy (how = How.ID, using = "terms_ok-error")
    private List<WebElement> errorTerms;

    // The create account button (Register Page)
    @FindBy (how = How.ID, using = "register_classic")
    private WebElement createAccount;


    /**This method contains the Register test procedure with valid and invalid inputs and then asserts the error messages
     * It also randomly selects the gender, newsletter and terms and conditions
     *
     * @param driver is the WebDriver
     * @param firstName is the user first name
     * @param lastName is the user last name
     * @param email is the user email
     * @param password is the user password
     * @param confPass is the confirmation password
     * @param errorGender is the error message for incorrect gender selection
     * @param errorFirstName is the error message for empty first name
     * @param errorLastName is the error message for empty last name
     * @param errorEmail is the error message for empty email
     * @param errorValidEmail is the error message for invalid email
     * @param errorPassword is the error message for empty password
     * @param errorValidPass is the error message for a less than 6 character password
     * @param errorConfirmPass is the error message for empty confirmation password
     * @param errorDifferentPass is the error message for different passwords
     * @param errorTerms is the error message when terms and conditions are not checked
     * @param screenshotNumber is an index variable used to differentiate the screenshot files when receiving data from the Data Provider
     * @throws IOException exception
     */
    public void register(WebDriver driver, String firstName, String lastName, String email, String password, String confPass, String errorGender, String errorFirstName, String errorLastName, String errorEmail, String errorValidEmail, String errorPassword, String errorValidPass, String errorConfirmPass, String errorDifferentPass, String errorTerms, String screenshotNumber) throws IOException {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0, 200)");
        ThreadSleeper.mySleeper(1000L);
        Random rand = new Random();
        int randGender = rand.nextInt(3) + 1;
        if (randGender == 1) {
            genderFemale.click();
        } else if (randGender == 2) {
            genderMale.click();
        } else {
            System.out.println("No gender selected");
        }
        this.firstName.clear();
        this.firstName.sendKeys(firstName);
        this.lastName.clear();
        this.lastName.sendKeys(lastName);
        this.email.clear();
        this.email.sendKeys(email);
        this.password.clear();
        this.password.sendKeys(password);
        this.confPass.clear();
        this.confPass.sendKeys(confPass);

        int randLetter = rand.nextInt(2) + 1;
        if (randLetter % 2 == 0) {
            newsletter.click();
        }
        int randTerms = rand.nextInt(2) + 1;
        if (randTerms % 2 == 0) {
            terms.click();
        }

        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_registerTest_" + (Integer.parseInt(screenshotNumber)+3) + ".png");
        createAccount.submit();

        ThreadSleeper.mySleeper(500L);
        if (this.errorGender.size() > 0) {
            if (this.errorGender.get(0).getText().equals("Bifeaza una din optiunile Dna. sau Dl.!")) {
                Assert.assertEquals(this.errorGender.get(0).getText(), errorGender);
            }
        }
        if (this.errorFirstName.size() > 0) {
            if (this.errorFirstName.get(0).getText().equals("Completeaza campul de prenume!")) {
                Assert.assertEquals(this.errorFirstName.get(0).getText(), errorFirstName);
            }
        }
        if (this.errorLastName.size() > 0) {
            if (this.errorLastName.get(0).getText().equals("Completeaza campul de nume!")) {
                Assert.assertEquals(this.errorLastName.get(0).getText(), errorLastName);
            }
        }
        if (this.errorEmail.size() > 0) {
            if (this.errorEmail.get(0).getText().equals("Introdu o adresa de email!")) {
                Assert.assertEquals(this.errorEmail.get(0).getText(), errorEmail);
            }
        }
        if (this.errorEmail.size() > 0) {
            if (this.errorEmail.get(0).getText().equals("Introdu o adresa de email valida!")) {
                Assert.assertEquals(this.errorEmail.get(0).getText(), errorValidEmail);
            }
        }
        if (this.errorPassword.size() > 0) {
            if (this.errorPassword.get(0).getText().equals("Introdu o parola valida!")) {
                Assert.assertEquals(this.errorPassword.get(0).getText(), errorPassword);
            }
        }
        if (this.errorPassword.size() > 0) {
            if (this.errorPassword.get(0).getText().equals("Parola trebuie sa aiba cel putin 6 caractere!")) {
                Assert.assertEquals(this.errorPassword.get(0).getText(), errorValidPass);
            }
        }
        if (this.errorConfirmPass.size() > 0) {
            if (this.errorConfirmPass.get(0).getText().equals("Confirma introducerea parolei!")) {
                Assert.assertEquals(this.errorConfirmPass.get(0).getText(), errorConfirmPass);
            }
        }
        if (this.errorConfirmPass.size() > 0) {
            if (this.errorConfirmPass.get(0).getText().equals("Parolele nu sunt identice!")) {
                Assert.assertEquals(this.errorConfirmPass.get(0).getText(), errorDifferentPass);
            }
        }
        if (this.errorTerms.size() > 0) {
            if (this.errorTerms.get(0).getText().equals("Pentru a te inregistra trebuie sa fii de acord cu termenii si conditiile site-ului!")) {
                Assert.assertEquals(this.errorTerms.get(0).getText(), errorTerms);
            }
        }

    }
}
