package com.elefant.automation.pages;

import com.elefant.automation.utils.ScreenshotGenerator;
import com.elefant.automation.utils.ThreadSleeper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;

// This class contains the Login and Logout test procedures
public class LoginLogoutPage {

    // The email field (Login Page)
    @FindBy (how = How.ID, using = "login_username")
    private WebElement email;

    // The password field (Login Page)
    @FindBy (how = How.ID, using = "login_password")
    private WebElement password;

    // The login button (Login Page)
    @FindBy (how = How.ID, using = "login_classic")
    private WebElement login;

    // The error message for both incorrect email and password (Login Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='form-box']/div[3]/div")
    private List<WebElement> credentialsError;

    // The error message for incorrect email (Login Page)
    @FindBy (how = How.ID, using = "login_username-error")
    private List<WebElement> emailError;

    // The error message for incorrect password (Login Page)
    @FindBy (how = How.ID, using = "login_password-error")
    private List<WebElement> passwordError;

    // The account menu selector (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='header-account-holder']/div[2]/span")
    private WebElement headerCat;

    // The account name from header, e.g. "Hi, Marius" (Main Page)
    @FindBy (how = How.ID, using = "user_name")
    private WebElement accountName;

    // The Logout option from the account menu (Main Page)
    @FindBy (how = How.ID, using = "logout-button")
    private WebElement logout;


    /**This method contains the Login test procedure with valid and invalid inputs and then asserts the error messages
     *
     * @param driver is the WebDriver
     * @param email is the user email
     * @param password is the user password
     * @param emailError is the error message for incorrect email
     * @param passwordError is the error message for incorrect password
     * @param credentialsError is the error message for both incorrect email and password
     * @param screenshotNumber is an index variable used to differentiate the screenshot files when receiving data from the Data Provider
     * @throws IOException exception
     */
    public void login(WebDriver driver, String email, String password, String emailError, String passwordError, String credentialsError, String screenshotNumber) throws IOException {
        this.email.clear();
        this.email.sendKeys(email);
        ThreadSleeper.mySleeper(500L);
        this.password.clear();
        this.password.sendKeys(password);

        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_loginTest_" + (Integer.parseInt(screenshotNumber)+3) + ".png");
        login.click();

        if (this.credentialsError.size() > 0) {
            if (this.credentialsError.get(0).getText().equals("Email-ul si/sau parola introduse sunt gresite.")) {
                Assert.assertEquals(this.credentialsError.get(0).getText(), credentialsError);
            }
        }

        if (this.emailError.size() > 0) {
            if (this.emailError.get(0).getText().equals("Introdu o adresa de email valida!")) {
                Assert.assertEquals(this.emailError.get(0).getText(), emailError);
            }
        }

        if (this.passwordError.size() > 0) {
            if (this.passwordError.get(0).getText().equals("Introdu o parola!")) {
                Assert.assertEquals(this.passwordError.get(0).getText(), passwordError);
            }
        }

        ThreadSleeper.mySleeper(500L);

    }

    // This method performs logout and then asserts that the user is not logged in anymore
    public void logout(WebDriver driver) {
        driver.navigate().refresh();
        ThreadSleeper.mySleeper(1000L);
        headerCat.click();
        logout.click();
        ThreadSleeper.mySleeper(1000L);
        Assert.assertFalse(accountName.getText().equals("Marius"));
    }
}
