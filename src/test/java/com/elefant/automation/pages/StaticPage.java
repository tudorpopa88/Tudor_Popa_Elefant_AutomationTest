package com.elefant.automation.pages;

import com.elefant.automation.utils.ScreenshotGenerator;
import com.elefant.automation.utils.ThreadSleeper;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.io.IOException;

// This class contains the Static Page testing procedure
public class StaticPage {

    // The about us section (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='homepage-footer-links']/li[1]/a")
    private WebElement aboutUs;

    // The authors section (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='homepage-footer-links']/li[2]/a")
    private WebElement authors;

    // The publishers section (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='homepage-footer-links']/li[3]/a")
    private WebElement publishers;

    // The special offers section (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='homepage-footer-links']/li[4]/a")
    private WebElement specialOffers;

    // The contact section (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='homepage-footer-links']/li[5]/a")
    private WebElement contact;

    // The about us text (About us Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='static_page']/div[1]/p[12]/span/strong")
    private WebElement aboutUsText;

    // The authors text (Authors Page)
    @FindBy (how = How.XPATH, using = ".//*[@id='content_cat']/div[3]/div[1]/div[2]/div[1]/div[1]")
    private WebElement authorsText;

    // The publishers text (Publishers Page)
    @FindBy (how = How.XPATH, using = ".//*[@id='content_cat']/div[1]/span")
    private WebElement publishersText;

    // The special offers text (Special offers Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='col-xs-12']/div[1]")
    private WebElement specialOffersText;

    // The contact text (Contact Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='static_page']/h1/span/span")
    private WebElement contactText;


    /**This method contains the static pages test procedure and asserts that some text elements are displayed on their corresponding pages
     *
     * @param driver is the WebDriver
     * @throws IOException exception
     */
    public void staticPage(WebDriver driver) throws IOException {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0, 4000)");
        ThreadSleeper.mySleeper(1500L);

        aboutUs.click();
        jse.executeScript("window.scrollBy(0, 200)");
        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_staticTest_1.png");
        Assert.assertTrue(aboutUsText.isDisplayed());
        driver.navigate().back();

        authors.click();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_staticTest_2.png");
        Assert.assertTrue(authorsText.isDisplayed());
        driver.navigate().back();

        publishers.click();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_staticTest_3.png");
        Assert.assertTrue(publishersText.isDisplayed());
        driver.navigate().back();

        specialOffers.click();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_staticTest_4.png");
        Assert.assertTrue(specialOffersText.isDisplayed());
        driver.navigate().back();

        contact.click();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_staticTest_5.png");
        Assert.assertTrue(contactText.isDisplayed());
        driver.navigate().back();
    }
}
