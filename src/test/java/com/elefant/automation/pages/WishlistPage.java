package com.elefant.automation.pages;

import com.elefant.automation.utils.ThreadSleeper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

// This class contains the Wishlist page test procedure
public class WishlistPage {

    // The new wishlist button (Wishlist Page)
    @FindBy (how = How.ID, using = "wishlistNou")
    private WebElement newWishlistButton;

    // The new wishlist input field (Wishlist Page)
    @FindBy (how = How.ID, using = "wishlist-nou")
    private WebElement newWishlistField;

    // The new wishlist save button (Wishlist Page)
    @FindBy (how = How.ID, using = "wishlistSave")
    private WebElement newWishlistSaveButton;

    // The list of the products titles (Wishlist Page)
    @FindBy (how = How.XPATH, using = ".//*[@id='right']/div[2]/div[*]/div[2]/div[1]/a")
    private List<WebElement> wishlistProductsTitleList;


    /**This method contains the Wishlist test procedure and asserts that the list of product titles
     * received from their corresponding pages is equal to the list of product titles displayed in the Wishlist Page
     *
     * @param productsTitleListFromPage is the list of product titles which belongs from their product pages
     */
    public void wishlist(List<String> productsTitleListFromPage) {
        newWishlistButton.click();
        newWishlistField.sendKeys("cadou Craciun");
        newWishlistSaveButton.click();
        ThreadSleeper.mySleeper(500L);
        int noOfProductTitles = productsTitleListFromPage.size();
        int noOfWishlistProducts = wishlistProductsTitleList.size();
        for (int i=0; i<noOfProductTitles; i++) {
            for (int j=0; j<noOfWishlistProducts; j++) {
                if (productsTitleListFromPage.get(i).equals(wishlistProductsTitleList.get(j).getText())) {
                    Assert.assertEquals(productsTitleListFromPage.get(j), wishlistProductsTitleList.get(i).getText());
                }
            }
        }
    }
}
