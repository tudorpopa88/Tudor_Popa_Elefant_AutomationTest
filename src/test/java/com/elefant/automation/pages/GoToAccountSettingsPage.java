package com.elefant.automation.pages;

import com.elefant.automation.utils.ScreenshotGenerator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.io.IOException;

// This class contains the navigation to Account Settings
public class GoToAccountSettingsPage {

    // The account menu selector (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='header-account-holder']/div[2]/span")
    private WebElement headerCat;

    // The account settings option from the account menu (Main Page)
    @FindBy (how = How.XPATH, using = ".//*[@class='dropdown-menu header-account-menu']/li[6]/a")
    private WebElement enterSettings;

    // The account settings title (Account Settings)
    @FindBy (how = How.XPATH, using = ".//*[@class='user-container']/div[2]")
    private WebElement accountSettingsTitle;


    /**This method navigates to Account Settings and then asserts that the title is displayed on the page
     *
     * @param driver is the WebDriver
     * @throws IOException exception
     */
    public void goToAccountSettings(WebDriver driver) throws IOException {
        headerCat.click();
        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_accountUpdateTest_1.png");
        enterSettings.click();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_accountUpdateTest_2.png");
        Assert.assertEquals(accountSettingsTitle.getText(), "Setari cont");
    }
}
