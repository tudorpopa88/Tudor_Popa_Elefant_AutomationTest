package com.elefant.automation.pages;

import com.elefant.automation.utils.ScreenshotGenerator;
import com.elefant.automation.utils.ThreadSleeper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;

// This class contains the Shopping basket test procedure
public class BasketPage {

    // The list of products (Basket Page)
    @FindBy(how = How.XPATH, using = ".//*[@class='product_cos_new']/div/div[2]/a")
    private List<WebElement> products;

    // The individual person selector (Basket Page)
    @FindBy(how = How.ID, using = "radio_1")
    private WebElement individualPerson;

    // The legal person selector (Basket Page)
    @FindBy(how = How.ID, using = "radio_2")
    private WebElement legalPerson;

    // The ONG selector (Basket Page)
    @FindBy(how = How.ID, using = "radio_3")
    private WebElement ong;

    // The last name field (Basket Page)
    @FindBy(how = How.ID, using = "nume")
    private WebElement lastName;

    // The first name field (Basket Page)
    @FindBy(how = How.ID, using = "prenume")
    private WebElement firstName;

    // The phone field (Basket Page)
    @FindBy(how = How.ID, using = "confirm_telefon")
    private WebElement phone;

    // The company name field (Basket Page)
    @FindBy(how = How.ID, using = "nume_firma")
    private WebElement companyName;

    // The CUI code field (Basket Page)
    @FindBy(how = How.ID, using = "cui")
    private WebElement cuiCode;

    // The bank field (Basket Page)
    @FindBy(how = How.ID, using = "banca")
    private WebElement bank;

    // The IBAN code field (Basket Page)
    @FindBy(how = How.ID, using = "iban")
    private WebElement ibanCode;

    // The company address (Basket Page)
    @FindBy(how = How.ID, using = "adresa_firma")
    private WebElement companyAddress;

    // The courier selector (Basket Page)
    @FindBy(how = How.ID, using = "tip_curier")
    private WebElement courier;

    // The personal lifting selector (Basket Page)
    @FindBy(how = How.ID, using = "transport_type_7")
    private WebElement personalLifting;

    // The country selector (Basket Page)
    @FindBy(how = How.ID, using = "tara_facturare")
    private WebElement country;

    // The county selector (Basket Page)
    @FindBy(how = How.ID, using = "judet_sector_facturare")
    private WebElement county;

    // The locality selector (Basket Page)
    @FindBy(how = How.ID, using = "oras_facturare")
    private WebElement locality;

    // The address details field (Basket Page)
    @FindBy(how = How.ID, using = "detalii_transport_comanda")
    private WebElement addressDetails;

    // The pickup point selector (Basket Page)
    @FindBy(how = How.ID, using = "order_pickup_point")
    private WebElement pickupPoint;

    // The cash payment selector (Basket Page)
    @FindBy(how = How.ID, using = "plata_cash")
    private WebElement cashPayment;

    // The online payment selector (Basket Page)
    @FindBy(how = How.ID, using = "plata_online")
    private WebElement onlinePayment;

    // The delivery notes field (Basket Page)
    @FindBy(how = How.ID, using = "observatii")
    private WebElement deliveryNotes;


    /**This method contains the basket test procedure
     * It asserts that the number of products displayed on the cart icon from Main Page is the same
     * with the number of products displayed in the Shopping Basket Page
     *
     * @param driver is the WebDriver
     * @param noOfProducts is the number of products displayed on the Main Page
     * @throws IOException exception
     */
    public void basket(WebDriver driver, int noOfProducts) throws IOException {
        int totalCartProducts = products.size();
        Assert.assertEquals(noOfProducts, totalCartProducts);

        legalPerson.click();
        phone.sendKeys(("4840802000"));
        companyName.sendKeys("Microsoft");
        cuiCode.sendKeys("6859662");
        bank.sendKeys("ING");
        ibanCode.sendKeys("RO73 INGB 0001 0000 0000 0777");
        companyAddress.sendKeys("Bentonville, Arkansas, U.S");
        Assert.assertFalse(individualPerson.isSelected());
        ScreenshotGenerator screenshotGenerator = new ScreenshotGenerator();
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_shoppingBasketTest_2.png");

        courier.click();
        Select countrySelector = new Select(country);
        countrySelector.selectByVisibleText("Romania");
        ThreadSleeper.mySleeper(500L);
        Select countySelector = new Select(county);
        countySelector.selectByVisibleText("Buzau");
        ThreadSleeper.mySleeper(500L);
        Select localitySelector = new Select(locality);
        localitySelector.selectByVisibleText("Mihailesti");
        addressDetails.sendKeys("Str. Soarelui, nr. 35, bl. B, sc. 2");

        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_shoppingBasketTest_3.png");

        onlinePayment.click();
        deliveryNotes.sendKeys("Sunt disponibil la adresa intre orele 10-14");
        screenshotGenerator.takeScreenshot(driver, "D:\\AUT_Screenshots\\screenshot_shoppingBasketTest_4.png");
    }
}
