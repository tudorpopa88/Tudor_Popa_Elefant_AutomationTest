package com.elefant.automation.pages;

import com.elefant.automation.utils.ThreadSleeper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

// This class contains the Login test account procedure
public class LoginTestAccountPage {

    // The email field (Login Page)
    @FindBy(how = How.ID, using = "login_username")
    private WebElement email;

    // The password field (Login Page)
    @FindBy (how = How.ID, using = "login_password")
    private WebElement password;

    // The login button (Login Page)
    @FindBy (how = How.ID, using = "login_classic")
    private WebElement login;


    /**This method contains the Login test account procedure with the valid input
     *
     * @param email is the user email
     * @param password is the user password
     */
    public void login(String email, String password) {
        this.email.clear();
        this.email.sendKeys(email);
        ThreadSleeper.mySleeper(500L);
        this.password.clear();
        this.password.sendKeys(password);
        login.click();
    }
}
