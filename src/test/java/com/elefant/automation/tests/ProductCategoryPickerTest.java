package com.elefant.automation.tests;

import com.elefant.automation.pages.ProductCategoryPickerPage;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

// This class contains the method for testing the Product Category Picker functionality
public class ProductCategoryPickerTest extends BaseTest {

    /**This method tests the Product Category Picker functionality
     * Test steps:
     * 1. Navigate to the main page
     * 2. Select a random main category and then a subcategory
     * 3. Pick a random product
     * 4. Add to cart
     *
     * @throws IOException exception
     * @see ProductCategoryPickerPage for more details
     */
    @Test
    public void productCategoryPickerTest() throws IOException {
        test = extent.startTest("Product Category Picker Test");
        Actions actions = new Actions(driver);
//      AvoidCampaign avoidCampaign = PageFactory.initElements(driver, AvoidCampaign.class);
        ProductCategoryPickerPage productCategoryPicker = PageFactory.initElements(driver, ProductCategoryPickerPage.class);
//      avoidCampaign.avoidCampaign();
        for (int i = 0; i < 3; i++) {
            productCategoryPicker.productCatPicker(actions, driver, i + 1);
            productCategoryPicker.productPagePicker(driver, i + 1);
            productCategoryPicker.addToCart(driver);
        }
        test.log(LogStatus.PASS, "Product Category Picker test passed");
        Assert.assertEquals(test.getRunStatus(), LogStatus.PASS);
    }
}
