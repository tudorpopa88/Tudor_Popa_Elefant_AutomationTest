package com.elefant.automation.tests;

import com.elefant.automation.pages.GoToLoginPage;
import com.elefant.automation.pages.LoginLogoutPage;
import com.elefant.automation.utils.SQLConnection;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

// This class contains the method for testing the Login/Logout functionality and a Data Provider
public class LoginLogoutTest extends BaseTest {

    /**This Data Provider method supplies data for the Login/Logout test method
     * It uses an Object[][] which connects to a separate SQL Connection class in order to retrieve the test data
     * @return data where each data Object can be assigned to the parameter list of the test method
     * @throws SQLException
     */
    @DataProvider
    public Object[][] LoginDataProvider() throws SQLException {
        SQLConnection sqlconn = new SQLConnection();
        int recordSize = 7;
        int columnSize = 7;
        Object[][] data = new Object[recordSize][columnSize];

        List<String> columnNameList = new ArrayList<String>();
        columnNameList.add("email");
        columnNameList.add("password");
        columnNameList.add("emailError");
        columnNameList.add("passwordError");
        columnNameList.add("credentialsError");
        columnNameList.add("runFlag");
        columnNameList.add("screenshotNumber");

        for (int i = 0; i < recordSize; i++) {
            for (int j = 0; j < columnSize; j++) {
                switch (columnNameList.get(j)) {
                    case "email": {
                        List<String> recordList = sqlconn.connectToSQL("SELECT * FROM login WHERE id = '" + (i + 1) + "'", columnSize, columnNameList.get(j), dbHost, dbPort, database, dbUsername, dbPassword);
                        data[i][j] = recordList.get(j);
                        break;
                    }
                    case "password": {
                        List<String> recordList = sqlconn.connectToSQL("SELECT * FROM login WHERE id = '" + (i + 1) + "'", columnSize, columnNameList.get(j), dbHost, dbPort, database, dbUsername, dbPassword);
                        data[i][j] = recordList.get(j);
                        break;
                    }
                    case "emailError": {
                        List<String> recordList = sqlconn.connectToSQL("SELECT * FROM login WHERE id = '" + (i + 1) + "'", columnSize, columnNameList.get(j), dbHost, dbPort, database, dbUsername, dbPassword);
                        data[i][j] = recordList.get(j);
                        break;
                    }
                    case "passwordError": {
                        List<String> recordList = sqlconn.connectToSQL("SELECT * FROM login WHERE id = '" + (i + 1) + "'", columnSize, columnNameList.get(j), dbHost, dbPort, database, dbUsername, dbPassword);
                        data[i][j] = recordList.get(j);
                        break;
                    }
                    case "credentialsError": {
                        List<String> recordList = sqlconn.connectToSQL("SELECT * FROM login WHERE id = '" + (i + 1) + "'", columnSize, columnNameList.get(j), dbHost, dbPort, database, dbUsername, dbPassword);
                        data[i][j] = recordList.get(j);
                        break;
                    }
                    case "runFlag": {
                        List<String> recordList = sqlconn.connectToSQL("SELECT * FROM login WHERE id = '" + (i + 1) + "'", columnSize, columnNameList.get(j), dbHost, dbPort, database, dbUsername, dbPassword);
                        data[i][j] = recordList.get(j);
                        break;
                    }
                    case "screenshotNumber": {
                        List<String> recordList = sqlconn.connectToSQL("SELECT * FROM login WHERE id = '" + (i + 1) + "'", columnSize, columnNameList.get(j), dbHost, dbPort, database, dbUsername, dbPassword);
                        data[i][j] = recordList.get(j);
                        break;
                    }
                    default: {
                        System.out.println("No such column name");
                        break;
                    }
                }
            }
        }
        return data;
    }


    /**This method tests the Login/Logout functionality
     * Test steps:
     * 1. Navigate to the main page
     * 2. Navigate to the Login Page
     * 3. Test the login box with different sets of data then login with the test account
     * 4. Logout from the test account
     *
     * @param email is the user email
     * @param password is the user password
     * @param emailError is the error message for incorrect email
     * @param passwordError is the error message for incorrect password
     * @param credentialsError is the error message for both incorrect email and password
     * @param runFlag is the variable used to differentiate the test steps when receiving data from the Data Provider
     * @param screenshotNumber is an index variable used to differentiate the screenshot files when receiving data from the Data Provider
     * @throws IOException exception
     * @see GoToLoginPage for more details
     * @see LoginLogoutPage for more details
     */
    @Test(dataProvider = "LoginDataProvider")
    public void loginLogoutTest(String email, String password, String emailError, String passwordError, String credentialsError, String runFlag, String screenshotNumber) throws IOException {
        test = extent.startTest("Login/Logout Test");
//      AvoidCampaign avoidCampaign = PageFactory.initElements(driver, AvoidCampaign.class);
        GoToLoginPage goToLogin = PageFactory.initElements(driver, GoToLoginPage.class);
        LoginLogoutPage loginLogout = PageFactory.initElements(driver, LoginLogoutPage.class);
        if (runFlag.equals("first")) {
            driver.get(host1);
//          avoidCampaign.avoidCampaign();
            goToLogin.goToLogin(driver);
        }
        loginLogout.login(driver, email, password, emailError, passwordError, credentialsError, screenshotNumber);
        if (runFlag.equals("last")) {
            loginLogout.logout(driver);
        }
        test.log(LogStatus.PASS, "Login/Logout test passed");
        Assert.assertEquals(test.getRunStatus(), LogStatus.PASS);
    }
}
