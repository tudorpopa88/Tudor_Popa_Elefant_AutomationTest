package com.elefant.automation.tests;

import com.elefant.automation.pages.*;
import com.elefant.automation.utils.ThreadSleeper;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

// This class contains the method for testing the Shopping Basket content
public class ShoppingBasketTest extends BaseTest {

    /**This method tests the Shopping Basket content
     * Test steps:
     * 1. Navigate to the main page
     * 2. Navigate to the Login Page
     * 3. Login with the test account
     * 4. Select a random main category and then a subcategory
     * 5. Pick 3 random products
     * 6. Navigate to the Shopping Basket
     * 7. Test the shopping basket with the order data
     *
     * @throws IOException exception
     * @see GoToLoginPage for more details
     * @see LoginTestAccountPage for more details
     * @see ProductCategoryPickerPage for more details
     * @see GoToBasketPage for more details
     * @see BasketPage for more details
     */
    @Test
    public void shoppingBasket() throws IOException {
        test = extent.startTest("Shopping Basket Test");
        Actions actions = new Actions(driver);
//      AvoidCampaign avoidCampaign = PageFactory.initElements(driver, AvoidCampaign.class);
        GoToLoginPage goToLogin = PageFactory.initElements(driver, GoToLoginPage.class);
        LoginTestAccountPage loginTestAccount = PageFactory.initElements(driver, LoginTestAccountPage.class);
        ProductCategoryPickerPage productCategoryPicker = PageFactory.initElements(driver, ProductCategoryPickerPage.class);
        GoToBasketPage goToBasket = PageFactory.initElements(driver, GoToBasketPage.class);
        BasketPage basket = PageFactory.initElements(driver, BasketPage.class);
//      avoidCampaign.avoidCampaign();
        goToLogin.goToLogin(driver);
        loginTestAccount.login("mariusdragomir150@gmail.com", "marius5050");
        productCategoryPicker.productCatPicker(actions, driver, 1);
        for (int i = 0; i < 3; i++) {
            productCategoryPicker.productPagePicker(driver, i + 1);
            productCategoryPicker.addToCart(driver);
            driver.navigate().back();
        }
        ThreadSleeper.mySleeper(2000L);
        int noOfProducts = goToBasket.goToBasket(driver);
        basket.basket(driver, noOfProducts);
        test.log(LogStatus.PASS, "Shopping Basket test passed");
        Assert.assertEquals(test.getRunStatus(), LogStatus.PASS);
    }
}
