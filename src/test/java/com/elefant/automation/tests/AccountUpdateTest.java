package com.elefant.automation.tests;

import com.elefant.automation.pages.*;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

// This class contains the method for testing the Account Update functionality and a Data Provider
public class AccountUpdateTest extends BaseTest {

    /**This Data Provider method supplies data for the Account Update test method
     *
     * @return data iterator where each String[] can be assigned to the parameter list of the test method
     */
    @DataProvider(name = "AccountUpdateDataProvider")
    public Iterator<Object[]> accountUpdateDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"", "", "", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Ai primit un mesaj pe adresa ta noua, te rugam sa validezi schimbarea adresei de email pentru contul elefant.ro.", "yes", "1"});
        dp.add(new String[]{"Eduard", "&fn4x18mH", "", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Ai primit un mesaj pe adresa ta noua, te rugam sa validezi schimbarea adresei de email pentru contul elefant.ro.", "no", "2"});
        dp.add(new String[]{"Eduard", "", "@4800@", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Ai primit un mesaj pe adresa ta noua, te rugam sa validezi schimbarea adresei de email pentru contul elefant.ro.", "no", "3"});
        dp.add(new String[]{"", "Toma", "eduard.toma.com", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Ai primit un mesaj pe adresa ta noua, te rugam sa validezi schimbarea adresei de email pentru contul elefant.ro.", "no", "4"});
        dp.add(new String[]{"", "", "eduard_toma20@gmail.com", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Ai primit un mesaj pe adresa ta noua, te rugam sa validezi schimbarea adresei de email pentru contul elefant.ro.", "no", "5"});
        dp.add(new String[]{"M81v*3CnH1<u^", "4&28dU%ZWP#2fg", "", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Ai primit un mesaj pe adresa ta noua, te rugam sa validezi schimbarea adresei de email pentru contul elefant.ro.", "no", "6"});
        dp.add(new String[]{"Eduard", "Toma", "eduard_toma20@gmail.com", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Ai primit un mesaj pe adresa ta noua, te rugam sa validezi schimbarea adresei de email pentru contul elefant.ro.", "no", "7"});

        return dp.iterator();
    }


    /**This method tests the Account Update functionality
     * Test steps:
     * 1. Navigate to the main page
     * 2. Navigate to the Login page
     * 3. Login with the test account
     * 4. Navigate to the Account Settings page
     * 5. Test the account settings boxes with different sets of data
     *
     * @param firstName is the user first name
     * @param lastName is the user last name
     * @param email is the user email
     * @param errorFirstName is the error message for empty first name
     * @param errorLastName is the error message for empty last name
     * @param errorEmail is the error message for empty email
     * @param errorValidEmail is the error message for incorrect email
     * @param emailChangeValid is the information message for email changing confirmation email
     * @param runFlag is the variable used to differentiate the test steps when receiving data from the Data Provider
     * @param screenshotNumber is an index variable used to differentiate the screenshot files when receiving data from the Data Provider
     * @throws IOException exception
     * @see GoToLoginPage for more details
     * @see LoginTestAccountPage for more details
     * @see GoToAccountSettingsPage for more details
     * @see AccountSettingsPage for more details
     */
    @Test(dataProvider = "AccountUpdateDataProvider")
    public void accountUpdateTest(String firstName, String lastName, String email, String errorFirstName, String errorLastName, String errorEmail, String errorValidEmail, String emailChangeValid, String runFlag, String screenshotNumber) throws IOException {
        test = extent.startTest("Account Update Test");
//      AvoidCampaign avoidCampaign = PageFactory.initElements(driver, AvoidCampaign.class);
        GoToLoginPage goToLogin = PageFactory.initElements(driver, GoToLoginPage.class);
        LoginTestAccountPage loginTestAccount = PageFactory.initElements(driver, LoginTestAccountPage.class);
        GoToAccountSettingsPage goToAccountSettings = PageFactory.initElements(driver, GoToAccountSettingsPage.class);
        AccountSettingsPage accountSettings = PageFactory.initElements(driver, AccountSettingsPage.class);
        if (runFlag.equals("yes")) {
            driver.get(host1);
//          avoidCampaign.avoidCampaign();
            goToLogin.goToLogin(driver);
            loginTestAccount.login("mariusdragomir150@gmail.com", "marius5050");
            goToAccountSettings.goToAccountSettings(driver);
        }
        accountSettings.accountSettings(driver, firstName, lastName, email, errorFirstName, errorLastName, errorEmail, errorValidEmail, emailChangeValid, screenshotNumber);

        test.log(LogStatus.PASS, "Account Update test passed");
        Assert.assertEquals(test.getRunStatus(), LogStatus.PASS);
    }
}
