package com.elefant.automation.tests;

import com.elefant.automation.pages.GoToRegisterPage;
import com.elefant.automation.pages.RegisterPage;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

// This class contains the method for testing the Register functionality and a Data Provider
public class RegisterTest extends BaseTest {

    /**This Data Provider method supplies data for the Register test method
     *
     * @return data iterator where each String[] can be assigned to the parameter list of the test method
     */
    @DataProvider(name = "RegisterDataProvider")
    public Iterator<Object[]> registerDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"", "", "", "", "", "Bifeaza una din optiunile Dna. sau Dl.!", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Introdu o parola valida!", "Parola trebuie sa aiba cel putin 6 caractere!", "Confirma introducerea parolei!", "Parolele nu sunt identice!", "Pentru a te inregistra trebuie sa fii de acord cu termenii si conditiile site-ului!", "yes", "1"});
        dp.add(new String[]{"Ion", "Popescu", "", "", "", "Bifeaza una din optiunile Dna. sau Dl.!", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Introdu o parola valida!", "Parola trebuie sa aiba cel putin 6 caractere!", "Confirma introducerea parolei!", "Parolele nu sunt identice!", "Pentru a te inregistra trebuie sa fii de acord cu termenii si conditiile site-ului!", "no", "2"});
        dp.add(new String[]{"", "", "ion45pop@gmail.com", "", "", "Bifeaza una din optiunile Dna. sau Dl.!", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Introdu o parola valida!", "Parola trebuie sa aiba cel putin 6 caractere!", "Confirma introducerea parolei!", "Parolele nu sunt identice!", "Pentru a te inregistra trebuie sa fii de acord cu termenii si conditiile site-ului!", "no", "3"});
        dp.add(new String[]{"", "", "", "alphadoctor", "alphadoctor", "Bifeaza una din optiunile Dna. sau Dl.!", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Introdu o parola valida!", "Parola trebuie sa aiba cel putin 6 caractere!", "Confirma introducerea parolei!", "Parolele nu sunt identice!", "Pentru a te inregistra trebuie sa fii de acord cu termenii si conditiile site-ului!", "no", "4"});
        dp.add(new String[]{"Ion", "", "ion45pop@", "", "", "Bifeaza una din optiunile Dna. sau Dl.!", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Introdu o parola valida!", "Parola trebuie sa aiba cel putin 6 caractere!", "Confirma introducerea parolei!", "Parolele nu sunt identice!", "Pentru a te inregistra trebuie sa fii de acord cu termenii si conditiile site-ului!", "no", "5"});
        dp.add(new String[]{"", "Popescu", "m3F80b4#w#(hi).33", "", "", "Bifeaza una din optiunile Dna. sau Dl.!", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Introdu o parola valida!", "Parola trebuie sa aiba cel putin 6 caractere!", "Confirma introducerea parolei!", "Parolele nu sunt identice!", "Pentru a te inregistra trebuie sa fii de acord cu termenii si conditiile site-ului!", "no", "6"});
        dp.add(new String[]{"n6V79x#G72%5~rT6Np", "4V8itf4*k7O58mwye2c", "", "doctor", "doctor", "Bifeaza una din optiunile Dna. sau Dl.!", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Introdu o parola valida!", "Parola trebuie sa aiba cel putin 6 caractere!", "Confirma introducerea parolei!", "Parolele nu sunt identice!", "Pentru a te inregistra trebuie sa fii de acord cu termenii si conditiile site-ului!", "no", "7"});
        dp.add(new String[]{"Ion", "Popescu", "ion45pop@gmail.com", "doc", "doc", "Bifeaza una din optiunile Dna. sau Dl.!", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Introdu o parola valida!", "Parola trebuie sa aiba cel putin 6 caractere!", "Confirma introducerea parolei!", "Parolele nu sunt identice!", "Pentru a te inregistra trebuie sa fii de acord cu termenii si conditiile site-ului!", "no", "8"});
        dp.add(new String[]{"George", "Stan", "8250", "doctor", "artist", "Bifeaza una din optiunile Dna. sau Dl.!", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Introdu o parola valida!", "Parola trebuie sa aiba cel putin 6 caractere!", "Confirma introducerea parolei!", "Parolele nu sunt identice!", "Pentru a te inregistra trebuie sa fii de acord cu termenii si conditiile site-ului!", "no", "9"});
        dp.add(new String[]{"George", "Stan", "geostan@yahoo.com", "n#72VY82mnP", "n#72VY82mnPG", "Bifeaza una din optiunile Dna. sau Dl.!", "Completeaza campul de prenume!", "Completeaza campul de nume!", "Introdu o adresa de email!", "Introdu o adresa de email valida!", "Introdu o parola valida!", "Parola trebuie sa aiba cel putin 6 caractere!", "Confirma introducerea parolei!", "Parolele nu sunt identice!", "Pentru a te inregistra trebuie sa fii de acord cu termenii si conditiile site-ului!", "no", "10"});
        return dp.iterator();
    }


    /**This method tests the Register functionality
     * Test steps:
     * 1. Navigate to the main page
     * 2. Navigate to the Register Page
     * 3. Test the register box with different sets of data
     *
     * @param firstName is the user first name
     * @param lastName is the user last name
     * @param email is the user email
     * @param password is the user password
     * @param confPass is the confirmation password
     * @param errorGender is the error message for incorrect gender selection
     * @param errorFirstName is the error message for empty first name
     * @param errorLastName is the error message for empty last name
     * @param errorEmail is the error message for empty email
     * @param errorValidEmail is the error message for invalid email
     * @param errorPassword is the error message for empty password
     * @param errorValidPass is the error message for a less than 6 characters password
     * @param errorConfirmPass is the error message for empty confirmation password
     * @param errorDifferentPass is the error message for different passwords
     * @param errorTerms is the error message when terms and conditions are not checked
     * @param runFlag is the variable used to differentiate the test steps when receiving data from the Data Provider
     * @param screenshotNumber is an index variable used to differentiate the screenshot files when receiving data from the Data Provider
     * @throws IOException exception
     * @see GoToRegisterPage for more details
     * @see RegisterPage for more details
     */
    @Test(dataProvider = "RegisterDataProvider")
    public void registerTest(String firstName, String lastName, String email, String password, String confPass, String errorGender, String errorFirstName, String errorLastName, String errorEmail, String errorValidEmail, String errorPassword, String errorValidPass, String errorConfirmPass, String errorDifferentPass, String errorTerms, String runFlag, String screenshotNumber) throws IOException {
        test = extent.startTest("Register Test");
//      AvoidCampaign avoidCampaign = PageFactory.initElements(driver, AvoidCampaign.class);
        GoToRegisterPage goToRegister = PageFactory.initElements(driver, GoToRegisterPage.class);
        RegisterPage register = PageFactory.initElements(driver, RegisterPage.class);
        if (runFlag.equals("yes")) {
            driver.get(host1);
//          avoidCampaign.avoidCampaign();
            goToRegister.goToRegister(driver);
        }
        register.register(driver, firstName, lastName, email, password, confPass, errorGender, errorFirstName, errorLastName, errorEmail, errorValidEmail, errorPassword, errorValidPass, errorConfirmPass, errorDifferentPass, errorTerms, screenshotNumber);

        test.log(LogStatus.PASS, "Register test passed");
        Assert.assertEquals(test.getRunStatus(), LogStatus.PASS);
    }
}
