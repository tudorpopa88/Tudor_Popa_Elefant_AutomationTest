package com.elefant.automation.tests;

import com.elefant.automation.utils.ExtentManager;
import com.elefant.automation.utils.WebBrowser;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

// This class contains the WebDriver interface declaration, the webpage load, the driver close and a Custom Report
public class BaseTest {

    WebDriver driver = null;
    WebBrowser.Browsers browserType;
    public static ExtentReports extent;
    public static ExtentTest test;
    public static String host1;
    public static String dbHost;
    public static String dbPort;
    public static String database;
    public static String dbUsername;
    public static String dbPassword;

    final String filePath = "Extent.html";

    // This method generates the Report HTML file
    @BeforeSuite
    public void beforeSuite() {
        extent = ExtentManager.getReporter(filePath);
    }

    // This method closes the Custom Report
    @AfterSuite
    protected void afterSuite() {
        extent.close();
    }

    /**
     * This method runs before any test method belonging to the classes configured to run
     * and starts the driver used (Firefox, Chrome, IE or HTMLUNIT)
     *
     * @see WebBrowser for more details
     */
    @BeforeTest
    @Parameters({"host", "dbHost", "dbPort", "database", "dbUsername", "dbPassword"})
    public void startDriver(String host, String dbHost, String dbPort, String database, String dbUsername, String dbPassword) {
        try {
            driver = WebBrowser.getDriver(WebBrowser.Browsers.CHROME);
            driver.manage().window().maximize();
            driver.get(host);
            host1 = host;
            this.dbHost = dbHost;
            this.dbPort = dbPort;
            this.database = database;
            this.dbUsername = dbUsername;
            this.dbPassword = dbPassword;

        } catch (Exception ex) {
            System.out.println("Browser specified is not on the supported list. Try Firefox, Chrome, IE or HTMLUNIT");
        }
    }

    /**This method is used to populate the Custom Report with Pass, Fail and Skip cases
     *
     * @param result
     */
    @AfterMethod
    public void reportMethod(ITestResult result) {

        try {
            if (result.getStatus() == ITestResult.FAILURE) {
                test.log(LogStatus.FAIL, "Test failed " + result.getTestClass());
            } else if (result.getStatus() == ITestResult.SKIP) {
                test.log(LogStatus.SKIP, "Test skipped " + result.getTestClass());
            } else {
//                System.out.println(test);
                test.log(LogStatus.PASS, "Test passed " + result.getTestClass());
            }
            extent.endTest(test);
            System.out.println("Test ended");
            extent.flush();

        } catch (Exception e) {
            System.out.println("Report exception " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * This method runs after all the test methods belonging
     * to the classes configured to run and closes the driver
     */
    @AfterTest
    public void closeDriver() {
        try {
            driver.close();
        } catch (Exception e) {
            System.out.println("No driver to close: " + e.getMessage());
        }
    }

}
