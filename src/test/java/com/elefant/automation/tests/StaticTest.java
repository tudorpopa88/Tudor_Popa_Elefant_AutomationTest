package com.elefant.automation.tests;

import com.elefant.automation.pages.StaticPage;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

// This class contains the method for testing the Static pages content
public class StaticTest extends BaseTest {

    /**This method tests the Static pages content
     * Test steps:
     * 1. Navigate to the main page
     * 2. Test the static pages content (About Us, Authors, Publishers, Special Offers and Contact)
     *
     * @throws IOException exception
     * @see StaticPage for more details
     */
    @Test
    public void staticTest() throws IOException {
        test = extent.startTest("Static Test");
//      AvoidCampaign avoidCampaign = PageFactory.initElements(driver, AvoidCampaign.class);
        StaticPage staticPage = PageFactory.initElements(driver, StaticPage.class);
//      avoidCampaign.avoidCampaign();
        staticPage.staticPage(driver);
        test.log(LogStatus.PASS, "Static test passed");
        Assert.assertEquals(test.getRunStatus(), LogStatus.PASS);
    }
}
