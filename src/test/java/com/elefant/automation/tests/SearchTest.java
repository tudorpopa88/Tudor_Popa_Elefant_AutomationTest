package com.elefant.automation.tests;

import com.elefant.automation.pages.SearchPage;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

// This class contains the method for testing the Search functionality and a Data Provider
public class SearchTest extends BaseTest {

    /**This Data Provider method supplies data for the Search test method
     *
     * @return data iterator where each String[] can be assigned to the parameter list of the test method
     */
    @DataProvider(name = "SearchDataProvider")
    public Iterator<Object[]> searchDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"ceas fossil", "Nici un produs gasit.", "1"});
        dp.add(new String[]{"versace", "Nici un produs gasit.", "2"});
        dp.add(new String[]{"enciclopedia britannica", "Nici un produs gasit.", "3"});
        dp.add(new String[]{"  joc catan   ", "Nici un produs gasit.", "4"});
        dp.add(new String[]{"royal canin 33", "Nici un produs gasit.", "5"});
        dp.add(new String[]{"lego", "Nici un produs gasit.", "6"});
        dp.add(new String[]{"", "Nici un produs gasit.", "7"});
        dp.add(new String[]{"65301382047", "Nici un produs gasit.", "8"});
        dp.add(new String[]{"vmeIdmlePxmqnOEoV", "Nici un produs gasit.", "9"});
        dp.add(new String[]{"vu9x2Bf*zr&n$pm~24b@wu#", "Nici un produs gasit.", "10"});
        return dp.iterator();
    }


    /**This method tests the Search functionality
     * Test steps:
     * 1. Navigate to the main page
     * 2. Test the Search box with different sets of data
     *
     * @param name is the search query
     * @param searchError is the search error message
     * @param screenshotNumber is an index variable used to differentiate the screenshot files when receiving data from the Data Provider
     * @throws IOException exception
     * @see SearchPage for more details
     */
    @Test(dataProvider = "SearchDataProvider")
    public void searchTest(String name, String searchError, String screenshotNumber) throws IOException {
        test = extent.startTest("Search Test");
//      AvoidCampaign avoidCampaign = PageFactory.initElements(driver, AvoidCampaign.class);
        SearchPage search = PageFactory.initElements(driver, SearchPage.class);
//      avoidCampaign.avoidCampaign();
        search.search(driver, name, searchError, screenshotNumber);
        test.log(LogStatus.PASS, "Search test passed");
        Assert.assertEquals(test.getRunStatus(), LogStatus.PASS);
    }
}
