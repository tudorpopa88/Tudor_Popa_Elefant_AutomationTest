package com.elefant.automation.tests;

import com.elefant.automation.pages.*;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// This class contains the test method for testing the Wishlist functionality
public class WishlistTest extends BaseTest {

    /**This method tests the Wishlist functionality
     * Test steps:
     * 1. Navigate to the main page
     * 2. Navigate to the Login Page
     * 3. Login with the test account
     * 4. Select a random main category and then a subcategory
     * 5. Pick 3 random products
     * 6. Add them to wishlist
     * 7. Navigate to Wishlist Page
     * 8. Test the wishlist functionality
     *
     * @throws IOException exception
     * @see GoToLoginPage for more details
     * @see LoginTestAccountPage for more details
     * @see ProductCategoryPickerPage for more details
     * @see GoToWishlistPage for more details
     * @see WishlistPage for more details
     */
    @Test
    public void wishlistTest() throws IOException {
        test = extent.startTest("Wishlist Test");
        Actions actions = new Actions(driver);
//      AvoidCampaign avoidCampaign = PageFactory.initElements(driver, AvoidCampaign.class);
        GoToLoginPage goToLogin = PageFactory.initElements(driver, GoToLoginPage.class);
        LoginTestAccountPage loginTestAccount = PageFactory.initElements(driver, LoginTestAccountPage.class);
        ProductCategoryPickerPage productCategoryPicker = PageFactory.initElements(driver, ProductCategoryPickerPage.class);
        GoToWishlistPage goToWishlist = PageFactory.initElements(driver, GoToWishlistPage.class);
        WishlistPage wishlist = PageFactory.initElements(driver, WishlistPage.class);

//      avoidCampaign.avoidCampaign();
        goToLogin.goToLogin(driver);
        loginTestAccount.login("mariusdragomir150@gmail.com", "marius5050");
        productCategoryPicker.productCatPicker(actions, driver, 1);
        List<String> productTitleList = new ArrayList<String>();
        for (int i = 0; i < 3; i++) {
            productCategoryPicker.productPagePicker(driver, i + 1);
            productTitleList.add(productCategoryPicker.addToWishlist(driver, actions));
            driver.navigate().back();
        }
        goToWishlist.goToWishlist(driver);
        wishlist.wishlist(productTitleList);

        test.log(LogStatus.PASS, "Wishlist test passed");
        Assert.assertEquals(test.getRunStatus(), LogStatus.PASS);
    }
}
