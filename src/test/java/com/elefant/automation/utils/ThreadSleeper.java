package com.elefant.automation.utils;

// This class contains the Thread Sleep command
public class ThreadSleeper {

    /**This method is used to put the thread activity to sleep for a certain amount of time
     *
     * @param timer is the sleep timer
     */
    public static void mySleeper(long timer) {
        try {
            Thread.sleep(timer);
        }
        catch (Exception ex) {
            System.out.println("Thread sleep error");
        }
    }
}
