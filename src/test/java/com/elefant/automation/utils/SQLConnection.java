package com.elefant.automation.utils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

// This class contains the SQL Connection procedure
public class SQLConnection {

    /**This method establishes the SQL connection
     * It can fit on any table size by relying on the given parameters
     *
     * @param query is the query syntax
     * @param columnSize is the number of columns
     * @param columnName in the name of the given column
     * @param dbHost is the database hostname e.g. "localhost"
     * @param dbPort is the database port number e.g. "3306"
     * @param database is the database name used in the test
     * @param dbUsername is the connected database user
     * @param dbPassword is the password for the connected user
     * @return Record list which is a row of data from the given table
     * @throws SQLException exception
     */
    public List<String> connectToSQL(String query, int columnSize, String columnName, String dbHost, String dbPort, String database, String dbUsername, String dbPassword) throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:mysql://" + dbHost + ":" + dbPort + "/" + database, dbUsername, dbPassword);
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.getResultSet();
        resultSet = statement.executeQuery(query);
        List<String> recordList = new ArrayList<String>();
        while (resultSet.next()) {
            for(int i=0; i<columnSize; i++) {
                recordList.add(resultSet.getString(columnName));
            }
        }
        resultSet.close();
        conn.close();
        return recordList;
    }
}
