package com.elefant.automation.utils;

import com.elefant.automation.tests.BaseTest;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

// This class contains the Screenshot generator procedure
public class ScreenshotGenerator extends BaseTest {

    /**This method generates a new screenshot to the desired path
     *
     * @param driver is the WebDriver
     * @param filePath is the file path
     * @throws IOException exception
     */
    public void takeScreenshot(WebDriver driver, String filePath) throws IOException {
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        File finalFile = new File(filePath);
        FileUtils.copyFile(screenshotFile, finalFile);
    }
}
