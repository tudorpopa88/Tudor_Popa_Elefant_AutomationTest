package com.elefant.automation.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

// This class contains the WebBrowser picker method
public class WebBrowser {

    public enum Browsers {
        FIREFOX,
        CHROME,
        IE,
        HTMLUNIT
    }

    /**This method is used to pass a WebDriver from enum to the Base Test class
     *
     * @param browser is the browser type
     * @return browser selection
     * @throws Exception exception
     */
    public static WebDriver getDriver (Browsers browser) throws Exception {
        WebDriver driver = null;

        switch (browser) {
            case FIREFOX: {
                System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
                return new FirefoxDriver();
            }
            case CHROME: {
                System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\drivers\\chromedriver.exe");
                return new ChromeDriver();
            }
            case IE: {
                System.setProperty("webdriver.ie.driver", "src\\main\\resources\\drivers\\IEDriverServer32.exe");
                return new InternetExplorerDriver();
            }
            case HTMLUNIT: {
                return new HtmlUnitDriver();
            }
            default: {
                throw new Exception("Invalid browser");
            }
        }
    }
}
