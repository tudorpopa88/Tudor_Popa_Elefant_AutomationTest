package com.elefant.automation.utils;

import com.relevantcodes.extentreports.ExtentReports;

// This class contains the Extent Manager
public class ExtentManager {
    private static ExtentReports extent;

    /**This method contains the Extent Reporter procedure
     *
     * @param filePath is the file path
     * @return extent
     */
    public synchronized static ExtentReports getReporter(String filePath) {
        if (extent == null) {
            extent = new ExtentReports(filePath, true);

            extent.addSystemInfo("Host Name", "http://www.elefant.ro").addSystemInfo("Environment", "QA");
        }
        return extent;
    }
}
